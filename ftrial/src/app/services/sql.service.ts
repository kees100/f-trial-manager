import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SqlService {

  constructor(private http: HttpClient) { }


  getTest():   Observable<any>{ 
    return this.http.get('http://192.168.143.72:10000/get-data');

  }
}
