import { Component } from '@angular/core';
import { SqlService } from 'src/app/services/sql.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent {
  test: any[];

  constructor(public sql: SqlService) {
    this.test = [];
  }


  ngOnInit() {
    this.sql.getTest().subscribe(data => {
      console.log(data);
      this.test = data;
      console.log(this.test);
    })

  }
}


